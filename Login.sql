create database NuevoUsuario;
use NuevoUsuario;

/*Se crea el usuario y su  coontrase�a, la contrase�a debe ir en comillas, aunque sea un entero*/
/*Con estas lineas se crea el usuario y se le dan todos los privilegios en la base de datos*/
create user vicman@localhost identified by '12345';
GRANT ALL PRIVILEGES ON newuser.* TO vicman@localhost;
 FLUSH PRIVILEGES;
 
 
 /*Una vez entramos con el login podemos ver las bases de datos que tenga ese usuario*/
 show databases;

use newuser;
create table Vic(nombre varchar(100) primary key);
insert into vic values ('Victor Manuel Mu�oz Azpeitia');
select * from vic;




/*Esta linea nos muestra una tabla con todos los usuarios de las bases de datos*/
select User from mysql.user;